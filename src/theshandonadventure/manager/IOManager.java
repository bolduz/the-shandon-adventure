package theshandonadventure.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import theshandonadventure.structure.Entity;
import theshandonadventure.structure.Cell;
import theshandonadventure.structure.Location;
import theshandonadventure.characters.Warrior;

public class IOManager {

	private static String oneTimeMessage = "";
	private static final int DISPLAYED_MAP_Y_SIZE = 10;
	private static final int DISPLAYED_MAP_X_SIZE = 30;

	public static void say(char message) {
		System.out.print(message);
	}

	public static void say(String message) {
		System.out.print(message);
	}

	public static void sayLine() {
		sayLine("");
	}

	public static void sayLine(String message) {
		System.out.println(message);
	}

	public static void setOneTimeMessage(String message) {
		if (oneTimeMessage.length() > 0) {
			oneTimeMessage += "\n";
		}
		oneTimeMessage += message;
	}

	public static boolean hasOneTimeMessage() {
		return oneTimeMessage.length() > 0;
	}

	public static void sayOneTimeMessage() {
		sayLine(oneTimeMessage);
		clearOneTimeMessage();
	}
	
	public static void clearOneTimeMessage(){
		oneTimeMessage = "";
	}

	public static String readLine() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = "";
		try {
			s = br.readLine();
		}
		catch (IOException ex) {
			Logger.getLogger(IOManager.class.getName()).log(Level.SEVERE, null, ex);
		}
		return s;
	}

	public static char readChar() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		char c = 0;
		try {
			c = (char) br.read();
		}
		catch (IOException ex) {
			Logger.getLogger(IOManager.class.getName()).log(Level.SEVERE, null, ex);
		}
		return c;
	}

	public static void clearScreen() {
		System.out.print("\033[H\033[2J");
		System.out.flush();
	}

	public static void displayMap(Location world, Warrior player) {
		int x = player.getX(), y = player.getY();
		int i, j;
		Cell currentCell;

		for (i = y + DISPLAYED_MAP_Y_SIZE; i > y - DISPLAYED_MAP_Y_SIZE; i--) {
			for (j = x - DISPLAYED_MAP_X_SIZE; j < x + DISPLAYED_MAP_X_SIZE; j++) {
				currentCell = world.at(j, i);
				if (j == x && i == y) {
					say(player.getMarker());
				}
				else if (currentCell != null) {
					say(currentCell.getMarker());
				}
				else {
					say('.');
				}
			}
			sayLine();
		}
	}

	public static void displayPlayerStatus(Warrior player) {
		String playerStatus = "%s is at [%d, %d]"
				+ "\nExp: %d, Speed: %.2f, Atk: %.2f"
				+ "\nLife: %d, Coins: %d, Weapon: %s";
		sayLine(String.format(playerStatus,
				player.getName(), player.getX(), player.getY(),
				player.getExpPoints(), player.getSpeed(), player.getAtkPower(),
				player.getLifePoints(), player.getCoins(), player.getWeaponName()
		));
	}
}
