package theshandonadventure.manager;

import java.awt.Point;
import java.util.Random;
import theshandonadventure.objects.Chest;
import theshandonadventure.structure.Entity;
import theshandonadventure.structure.Location;
import theshandonadventure.structure.Cell;
import theshandonadventure.objects.Ogre;
import theshandonadventure.objects.Potion;
import theshandonadventure.characters.FactoryCreature;
import theshandonadventure.characters.Warrior;
import theshandonadventure.objects.Weapon;

public class GameManager {

	private static final int ENTITY_GENERATION_PROBABILITY = 30; //%
	private static final int ESCAPE_BASE_PROBABILITY = 30; //%
	private Warrior player;
	private Location location;
	private Point spawnPoint;
	private static GameManager game;

	private GameManager() {
	}

	public static GameManager create() {
		if (game == null) {
			game = new GameManager();
		}
		return game;
	}

	public void initialize() {
		// Show game introduction
		this.showIntro();
		// Create the world
		location = new Location("World");
		spawnPoint = location.randomPosition();

		// Create the first city, acting as spawn point for the player
		Location shandon = new Location("Shandon");

		location.addEntity(shandon, spawnPoint.x, spawnPoint.y);

		// Create the player
		this.createPlayer();
	}

	private void showIntro() {
		IOManager.sayLine("Welcome to The Shandon Adventure!");
	}

	private void createPlayer() {
		String playerName;
		char choice;

		IOManager.say("Before starting your journey, pick a name for your character: ");
		playerName = IOManager.readLine();
		IOManager.sayLine("Choose a warrior category: ");
		IOManager.sayLine(" 1) Paladin");
		IOManager.sayLine(" 2) Thief");
		IOManager.sayLine(" 3) Barbarian");

		do {
			choice = IOManager.readChar();
		} while (choice < '1' || choice > '3');

		switch (choice) {
			case '1':
				this.player = FactoryCreature.createPaladin();
				break;
			case '2':
				this.player = FactoryCreature.createThief();
				break;
			case '3':
				this.player = FactoryCreature.createBarbarian();
				break;
		}

		this.player.setName(playerName);

		this.player.setPosition(spawnPoint);
	}

	public void addRandomEntity(Location loc, Point currentPos) {
		/*
			30% to generate entity
			70% to leave cell empty (thus, t == null)
		 */
		Entity newEntity = null;
		int generationChance = (int) (Math.random() * 100) + 1;
		if (generationChance <= ENTITY_GENERATION_PROBABILITY) {
			int entityType = (int) (Math.random() * 6) + 1;
			switch (entityType) {
				case 1:
					newEntity = new Weapon("Sword", 10);
					break;
				case 2:
					newEntity = new Weapon("Bow", 15);
					break;
				case 3:
					newEntity = new Weapon("Dagger", 20);
					break;
				case 4:
					newEntity = new Potion();
					break;
				case 5:
					newEntity = new Chest();
					break;
				case 6:
					newEntity = new Ogre();
					break;
				default:
					break;
			}
		}
		location.addEntity(newEntity, currentPos.x, currentPos.y);
	}

	public void startAdventure() {
		Point currentPos;
		Cell currentCell;
		Entity currentCellEntity;
		char choice;
		boolean userQuit = false;
		boolean theresAnEnemy;
		boolean theresAWeapon;
		boolean theresAnObject;
		boolean escapeSuccess = false; // Init for first iteration, will be reassigned after
		Random escapeProbabilityGenerator = new Random();

		IOManager.sayLine("Welcome " + player.getName() + ".");
		IOManager.sayLine("Your journey has now begun.");
		IOManager.sayLine("");

		while (player.isAlive() && !userQuit) {
			IOManager.sayLine("Current location: " + location.getName());
			IOManager.displayMap(location, player);
			IOManager.displayPlayerStatus(player);

			if (IOManager.hasOneTimeMessage()) {
				IOManager.sayOneTimeMessage();
			}

			theresAnEnemy = false;
			theresAWeapon = false;
			theresAnObject = false;

			currentPos = player.getPosition();
			if (!currentPos.equals(spawnPoint)) {
				// Try to add a random entity to the player's cell
				addRandomEntity(location, currentPos);
			}
			currentCell = location.at(currentPos);

			currentCellEntity = currentCell.getEntity();
			if (currentCellEntity instanceof Location) {
				IOManager.sayLine("[i] There's a city named " + currentCellEntity.getName() + " here.");
			}
			else if (currentCellEntity instanceof Ogre) {
				if (!escapeSuccess) {
					theresAnEnemy = true;
					IOManager.sayLine("[!] There's an Ogre here. Be careful!");
				}
			}
			else if (currentCellEntity != null) {
				if (currentCellEntity instanceof Weapon) {
					theresAWeapon = true;
				}
				else if (currentCellEntity instanceof Potion || currentCellEntity instanceof Chest) {
					theresAnObject = true;
				}
				IOManager.sayLine("[i] There's a " + currentCellEntity.getName() + " here.");
			}

			IOManager.sayLine();
			IOManager.sayLine("Possible actions:");

			if (theresAnEnemy) {
				IOManager.sayLine("\tf) Fight");
				IOManager.sayLine("\te) Escape");
			}
			else {
				if (!currentPos.equals(spawnPoint)) {
					IOManager.sayLine("\tt) Teleport to Shandon City");
				}
				IOManager.sayLine("\tw) Move up");
				IOManager.sayLine("\ts) Move down");
				IOManager.sayLine("\ta) Move left");
				IOManager.sayLine("\td) Move right");

				if (theresAWeapon) {
					IOManager.sayLine("\tc) Catch/Change the weapon");
				}
				else if (theresAnObject) {
					IOManager.sayLine("\tu) Use the tool");
				}
				else if (player.hasWeapon() && !escapeSuccess) { // Can't drop weapon if there's an enemy
					IOManager.sayLine("\tl) Drop the weapon");
				}
			}

			IOManager.sayLine("\tq) Quit game");
			IOManager.say("Your choice: ");

			choice = IOManager.readChar();

			if (choice == 'q') {
				// User can always close the game
				userQuit = true;
			}
			else if (theresAnEnemy) {
				if (choice == 'e') {
					// Escape
					// The player has a 30% chance to escape, which increases/decreases based on his speed
					escapeSuccess = (escapeProbabilityGenerator.nextInt(100) + 1) < ESCAPE_BASE_PROBABILITY * player.getSpeed();
					if (escapeSuccess) {
						IOManager.setOneTimeMessage("[i] Escape successful! Now RUN! The Ogre is still here...");
					}
					else {
						IOManager.setOneTimeMessage("[x] You failed to escape and had to fight...");
						choice = 'f';
					}
				}
				if (choice == 'f') {
					// Fight!
					((Ogre) currentCellEntity).fight(player); // theresAnEnemy impedisce che entity sia null
					player.increaseExp();
					currentCell.setEntity(null);
				}
			}
			else if (choice == 'u' && theresAnObject) {
				// Use the object/tool
				if (currentCellEntity instanceof Chest) {
					((Chest) currentCellEntity).use(player);
				}
				else if (currentCellEntity instanceof Potion) {
					((Potion) currentCellEntity).use(player);
					currentCell.setEntity(null);
				}
			}
			else if (choice == 'c' && theresAWeapon) {
				// Change weapon
				if (player.hasWeapon()) {
					currentCell.setEntity(player.getWeapon()); // Drop current weapon
				}
				else {
					currentCell.setEntity(null); // Pickup weapon
				}
				player.setWeapon((Weapon) currentCellEntity);
			}
			else if (choice == 'l' && player.hasWeapon() && currentCellEntity == null) {
				// Drop weapon
				currentCell.setEntity(player.getWeapon());
				player.setWeapon(null);
			}
			else {
				// Move, or teleport
				boolean isValidCommand = true;
				switch (choice) {
					case 'w':
						player.moveUp();
						break;
					case 'a':
						player.moveLeft();
						break;
					case 's':
						player.moveDown();
						break;
					case 'd':
						player.moveRight();
						break;
					case 't':
						player.setPosition(spawnPoint);
						break;
					default:
						isValidCommand = false;
						break;
				}

				if (isValidCommand) {
					// Reset only if command is valid
					escapeSuccess = false;
				}
			}
			if (player.isAlive() && !userQuit)
				IOManager.clearScreen();
		}
		IOManager.sayOneTimeMessage();
		IOManager.sayLine("GAME OVER!");
	}
}
