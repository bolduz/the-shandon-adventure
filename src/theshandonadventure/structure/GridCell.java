package theshandonadventure.structure;

class GridCell implements Cell {

	private Entity entity;

	@Override
	public void setEntity(Entity e) {
		this.entity = e;
	}

	@Override
	public boolean isEmpty() {
		return entity == null;
	}

	@Override
	public Entity getEntity() {
		return entity;
	}

	@Override
	public char getMarker() {
		return entity == null ? ' ' : entity.getMarker();
	}
}
