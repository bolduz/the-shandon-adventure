package theshandonadventure.structure;

import java.awt.Point;
import java.util.HashMap;
import java.util.Map;

public class Location extends Entity {

	private static final int SIZE_MAX = 50;  // Maximum map size
	private Map<Point, GridCell> map; // Location's map

	public Location(String name) {
		super('C', name);
		this.map = new HashMap<>();
	}

	public Map<Point, GridCell> getMap() {
		return this.map;
	}

	public Location setMap(Map<Point, GridCell> m) {
		this.map = m;
		return this;
	}

	public GridCell at(Point p) {
		return this.map.containsKey(p) ? this.map.get(p) : null;
	}

	public GridCell at(int x, int y) {
		return at(new Point(x, y));
	}

	public Location addEntityAtRandomPosition(Entity entity) {
		int x = randomInt();
		int y = randomInt();

		this.addEntity(entity, x , y);
		return this;
	}

	public Location addEntity(Entity entity, int x, int y) {
		GridCell destination = this.at(x, y);
		if (destination == null) {
			GridCell cell = new GridCell();
			cell.setEntity(entity);
			this.map.put(new Point(x, y), cell);
		}
		return this;
	}

	public Point randomPosition() {
		return new Point(randomInt(), randomInt());
	}

	private int randomInt() {
		return (int) (Math.random() * SIZE_MAX);
	}
}
