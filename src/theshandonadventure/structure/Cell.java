package theshandonadventure.structure;

public interface Cell {

	abstract void setEntity(Entity e);

	abstract boolean isEmpty();

	abstract Entity getEntity();

	abstract char getMarker();
}
