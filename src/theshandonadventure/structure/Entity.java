package theshandonadventure.structure;

import java.awt.Point;

public abstract class Entity {

	private char marker;
	private String name;

	public Entity(String name) {
		this(' ', name);
	}

	public Entity(char marker, String name) {
		this.marker = marker;
		this.name = name;
	}


	public char getMarker() {
		return this.marker;
	}

	public String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	public void setMarker(char marker) {
		this.marker = marker;
	}

	public void setName(String name) {
		this.name = name;
	}

}
