package theshandonadventure.objects;

import theshandonadventure.characters.Warrior;
import theshandonadventure.structure.Entity;
import theshandonadventure.manager.IOManager;

public class Ogre extends Entity {

	public Ogre() {
		super("Ogre");
	}

	public void fight(Warrior p) {
		int lifePoints = p.getLifePoints();
		p.addLifePoints((int) (-p.getWeaponPower() / p.getAtkPower()));

		if (p.isAlive()) {
			IOManager.setOneTimeMessage("[-] The Ogre was defeated! You lost " + (lifePoints - p.getLifePoints()) + " life points.");
		}
		else {
			IOManager.setOneTimeMessage("[!] The Ogre has won! You are dead!");
		}
	}
}
