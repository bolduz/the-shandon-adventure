package theshandonadventure.objects;

import theshandonadventure.structure.Entity;

public class Weapon extends Entity {

	public static final String NO_WEAPON_DESCRIPTION = "Barehanded";
	private final int power;

	public Weapon(String name, int power) {
		super(name);
		this.power = power;
	}

	public int getPower() {
		return power;
	}
}
