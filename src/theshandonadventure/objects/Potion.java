package theshandonadventure.objects;

import theshandonadventure.characters.Warrior;
import theshandonadventure.structure.Entity;
import theshandonadventure.manager.IOManager;

public class Potion extends Entity {

	private final int lifePointsIncreaseAmount;

	public Potion() {
		super("Potion");
		lifePointsIncreaseAmount = 10;
	}

	public void use(Warrior p) {
		p.addLifePoints(lifePointsIncreaseAmount);
		IOManager.setOneTimeMessage("[+] Life points increased by 10!");
	}
}
