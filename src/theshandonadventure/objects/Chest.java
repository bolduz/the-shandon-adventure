package theshandonadventure.objects;

import theshandonadventure.characters.Warrior;
import theshandonadventure.structure.Entity;
import theshandonadventure.manager.IOManager;
import java.util.Random;

public class Chest extends Entity {

	private static final int RANDOM_MAX = 5;
	private static final int ADD_COINS_CHOICE_LIMIT = RANDOM_MAX / 2 + 1;
	private static final double COINS_WEIGHT_DIVIDER = 100.0;
	/*
		The following legend should be used when generating the chest content:
			1) Add 10 coins
			2) Add 20 coins
			3) Add 30 coins
			4) Poison! Decrease life points by 10
			5) Empty... You'll be luckier next time ;)
	 */
	private int lifePointsDecreaseAmount;
	private int coinsIncrementAmount;

	public Chest() {
		super("Chest");
		this.lifePointsDecreaseAmount = 0;
		this.coinsIncrementAmount = 0;

		int choice = new Random().nextInt(RANDOM_MAX) + 1;
		if (choice <= ADD_COINS_CHOICE_LIMIT) {
			this.coinsIncrementAmount = choice * 10;
		}
		else if (choice > ADD_COINS_CHOICE_LIMIT && choice < RANDOM_MAX) {
			this.lifePointsDecreaseAmount = 10;
		}
	}

	public void use(Warrior p) {
		if (lifePointsDecreaseAmount == 0 && coinsIncrementAmount == 0) {
			IOManager.setOneTimeMessage("[i] The chest is empty!");
		}
		else if (coinsIncrementAmount > 0) {
			p.addCoins(coinsIncrementAmount);
			IOManager.setOneTimeMessage("[+] Found " + coinsIncrementAmount + " coins!");
		}
		else if (lifePointsDecreaseAmount > 0) {
			p.addLifePoints(-lifePointsDecreaseAmount);
			IOManager.setOneTimeMessage("[-] That was poison! Life points decreased by " + lifePointsDecreaseAmount + ".");
		}

		p.decreaseSpeed(coinsIncrementAmount / COINS_WEIGHT_DIVIDER);

		coinsIncrementAmount = 0;
		lifePointsDecreaseAmount = 0;
	}
}
