package theshandonadventure;

import theshandonadventure.manager.GameManager;

public class TheShandonAdventure {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		GameManager gm = GameManager.create();

		/* Create world, add creatures */
		gm.initialize();

		/* Start the adventure! */
		gm.startAdventure();
	}
}
