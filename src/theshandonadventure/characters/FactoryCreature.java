package theshandonadventure.characters;

public class FactoryCreature {

	private FactoryCreature() {
	}

	public static Warrior createPaladin() {
		return new Paladin();
	}

	public static Warrior createBarbarian() {
		return new Barbarian();
	}

	public static Warrior createThief() {
		return new Thief();
	}
}
