package theshandonadventure.characters;

class Paladin extends Warrior {

	/*
		Paladin
		Life points: 100
		Speed: 1
		Atk: 1
	 */
	public Paladin() {
		super('P', "Paladin", 1, 1);
	}
}
