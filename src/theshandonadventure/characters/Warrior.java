package theshandonadventure.characters;

import theshandonadventure.objects.Weapon;

public abstract class Warrior extends Creature {

	private static final double STATS_INCREASE_ON_LEVELUP = 0.2;
	private static final int LEVELUP_EXP = 10;
	private Weapon weapon;
	private double speed;
	private double atkPower;
	private int lifePoints;
	private int expPoints;
	private int coins;

	public Warrior(char marker, String name) {
		this(marker, name, 1, 1);
	}

	public Warrior(char marker, String name, double speed, double atk) {
		super(marker, name);
		this.lifePoints = 100;
		this.speed = speed;
		this.atkPower = atk;
		this.expPoints = 0;
		this.coins = 0;
		this.weapon = null;
	}

	public boolean isAlive() {
		return lifePoints > 0;
	}

	public int getLifePoints() {
		return lifePoints;
	}

	public int getCoins() {
		return coins;
	}

	public String getWeaponName() {
		return weapon == null ? Weapon.NO_WEAPON_DESCRIPTION : weapon.getName();
	}

	public Weapon getWeapon() {
		return weapon;
	}

	public void setWeapon(Weapon w) {
		weapon = w;
	}

	public void addCoins(int c) {
		coins += c;
	}

	public void addLifePoints(int l) {
		lifePoints += l;
	}

	public boolean hasWeapon() {
		return weapon != null;
	}

	public int getExpPoints() {
		return expPoints;
	}

	public void increaseExp() {
		expPoints++;
		if (expPoints % LEVELUP_EXP == 0) {
			levelUp();
		}
	}

	private void levelUp() {
		speed += STATS_INCREASE_ON_LEVELUP;
		atkPower += STATS_INCREASE_ON_LEVELUP;
	}

	public double getSpeed() {
		return speed;
	}

	public double getAtkPower() {
		return atkPower;
	}

	public void decreaseSpeed(double v) {
		speed -= v;
		if (speed < 0) {
			speed = 0;
		}
	}

	public int getWeaponPower() {
		if (weapon == null) {
			return 40;
		}
		return weapon.getPower();
	}
}
