package theshandonadventure.characters;

class Thief extends Warrior {

	/*
		Thief
		Life points: 100
		Speed: 2
		Atk: 0.5
	 */
	public Thief() {
		super('T', "Thief", 2, 0.5);
	}
}
