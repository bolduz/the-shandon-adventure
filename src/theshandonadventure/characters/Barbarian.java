package theshandonadventure.characters;

class Barbarian extends Warrior {

	/*
		Barbarian
		Life points: 100
		Speed: 0.5
		Atk: 2
	 */
	public Barbarian() {
		super('B', "Barbarian", 0.5, 2);
	}
}
