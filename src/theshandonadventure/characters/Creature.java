package theshandonadventure.characters;

import theshandonadventure.structure.Entity;

import java.awt.Point;

class Creature extends Entity {

	private static final int STEP = 1;

	private Point position;

	public Creature(char marker, String name) {
		super(marker, name);
	}

	public int getX() {
		return (int) this.position.getX();
	}

	public int getY() {
		return (int) this.position.getY();
	}

	public Point getPosition() {
		return (Point) this.position.clone();
	}

	public void setPosition(int x, int y) {
		this.position.setLocation(x, y);
	}

	public void setPosition(Point p) {
		this.position = (Point) p.clone();
	}

	public Creature moveUp() {
		this.position.translate(0, STEP);
		return this;
	}

	public Creature moveDown() {
		this.position.translate(0, -STEP);
		return this;
	}

	public Creature moveLeft() {
		this.position.translate(-STEP, 0);
		return this;
	}

	public Creature moveRight() {
		this.position.translate(STEP, 0);
		return this;
	}
}
