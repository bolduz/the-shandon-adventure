package theshandonadventure.objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import theshandonadventure.characters.FactoryCreature;
import theshandonadventure.characters.Warrior;

/**
 *
 * @author Enrico
 */
public class PotionTest {
	@Test
	public void testUse() {
		// Test del metodo [use] della classe [Potion]
		Warrior player = FactoryCreature.createPaladin();
		Potion p = new Potion();
		
		int startLifePoints = player.getLifePoints();
		p.use(player);
		int endLifePoints = player.getLifePoints();
		
		assertTrue(endLifePoints > startLifePoints);
		assertEquals(startLifePoints, endLifePoints - 10);
	}
	
}
