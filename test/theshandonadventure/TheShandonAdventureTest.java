/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package theshandonadventure;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import theshandonadventure.characters.FactoryCreature;
import theshandonadventure.characters.Warrior;

/**
 *
 * @author Enrico
 */
public class TheShandonAdventureTest {

	@Test
	public void testWarriorsStats() {
		Warrior thief = FactoryCreature.createThief();
		Warrior paladin = FactoryCreature.createPaladin();
		Warrior barbarian = FactoryCreature.createBarbarian();
		
		assertTrue(thief.getSpeed() > paladin.getSpeed());
		assertTrue(paladin.getSpeed() > barbarian.getSpeed());
		
		assertTrue(barbarian.getAtkPower() > paladin.getAtkPower());		
		assertTrue(paladin.getAtkPower() > thief.getAtkPower());
		
		assertEquals(thief.getLifePoints(), paladin.getLifePoints(), barbarian.getLifePoints());
	}
	
}
