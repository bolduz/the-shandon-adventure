package theshandonadventure.manager;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Enrico
 */
public class GameManagerTest {
	@Test
	public void testCreate() {
		// "Test del metodo [create], classe [GameManager]
		// "GameManager.create() non deve restituire null
		GameManager gm = GameManager.create();
		assertNotNull(gm);
		
		// GameManager è una classe Singleton, quindi due chiamate al metodo create
		// devono restituire la stessa istanza
		GameManager gm2 = GameManager.create();
		assertEquals(gm, gm2);
	}
}
