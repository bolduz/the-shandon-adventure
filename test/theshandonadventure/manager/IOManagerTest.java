package theshandonadventure.manager;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Enrico
 */
public class IOManagerTest {
	@Test
	public void testOneTimeMessage() {
		// Test del metodo [setOneTimeMessage], classe [IOManager]
		// Il messaggio iniziale deve essere vuoto
		assertFalse(IOManager.hasOneTimeMessage());
		
		// Impostando un messaggio hasOneTimeMessage deve ritornare True
		IOManager.setOneTimeMessage("Messaggio di test");
		assertTrue(IOManager.hasOneTimeMessage());
		
		// Tramite clearOneTimeMessage rimuovo il messaggio precedente
		IOManager.clearOneTimeMessage();
		assertFalse(IOManager.hasOneTimeMessage());
		
		// Inoltre una volta visualizzato il messaggio esso deve esser resettato
		IOManager.setOneTimeMessage("Terzo messaggio di test");
		assertTrue(IOManager.hasOneTimeMessage());
		IOManager.sayOneTimeMessage();
		assertFalse(IOManager.hasOneTimeMessage());
	}
}
