package theshandonadventure.characters;

import java.awt.Point;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Enrico
 */
public class CreatureTest {
	@Test
	public void testConstructor() {
		// Test del costruttore della classe [Creature]
		Creature creature = new Creature('c', "Test creature");
		assertNotNull(creature);
		assertEquals(creature.getName(), "Test creature");
		assertEquals(creature.getMarker(), 'c');
	}
	
	@Test
	public void testSetPosition(){
		// Test del metodo [setPosition] della classe [Creature]
		Creature creature = new Creature('c', "Test creature");
		creature.setPosition(new Point(10, 20));
		assertEquals(creature.getX(), 10);
		assertEquals(creature.getY(), 20);
		
		creature.setPosition(new Point(15, 25));
		assertEquals(creature.getX(), 15);
		assertEquals(creature.getY(), 25);
	}
	
	@Test
	public void testMovement(){
		int startX = 10, startY = 20;
		Creature creature = new Creature('c', "Test creature");
		
		// Test del metodo [moveUp] della classe [Creature]
		creature.setPosition(new Point(startX, startY));
		creature.moveUp();
		assertTrue(creature.getX() == startX);
		assertTrue(creature.getY() == startY + 1);
				
		// Test del metodo [moveDown] della classe [Creature]
		creature.setPosition(startX, startY);
		creature.moveDown();
		assertTrue(creature.getX() == startX);
		assertTrue(creature.getY() == startY - 1);
				
		// Test del metodo [moveLeft] della classe [Creature]
		creature.setPosition(startX, startY);
		creature.moveLeft();
		assertTrue(creature.getX() == startX - 1);
		assertTrue(creature.getY() == startY);
				
		// Test del metodo [moveRight] della classe [Creature]
		creature.setPosition(startX, startY);
		creature.moveRight();
		assertTrue(creature.getX() == startX + 1);
		assertTrue(creature.getY() == startY);
	}
}
