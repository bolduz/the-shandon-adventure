package theshandonadventure.characters;

import org.junit.Test;
import static org.junit.Assert.*;
import theshandonadventure.objects.Ogre;
import theshandonadventure.objects.Weapon;

/**
 *
 * @author Enrico
 */
public class WarriorTest {

	@Test
	public void testAddLifePoints(){
		Warrior player = FactoryCreature.createThief();
		int startLifePoints = player.getLifePoints();
		player.addLifePoints(10);
		int incrementedLifePoints = player.getLifePoints();
		player.addLifePoints(-10);
		int decrementedLifePoints = player.getLifePoints();
		
		assertTrue(startLifePoints < incrementedLifePoints);
		assertTrue(decrementedLifePoints < incrementedLifePoints);
		assertEquals(startLifePoints, decrementedLifePoints);
		
		assertEquals(incrementedLifePoints, startLifePoints + 10);
		assertEquals(decrementedLifePoints, incrementedLifePoints - 10);
	}
	
	@Test
	public void testBarehandedFight() {
		Warrior player = FactoryCreature.createThief();
		Ogre ogre = new Ogre();

		int startLifePoints = player.getLifePoints();
		ogre.fight(player);
		int endLifePoints = player.getLifePoints();
		assertTrue(startLifePoints > endLifePoints);

		// Il ladro muore dopo un paio di battaglie
		ogre.fight(player);
		assertFalse(player.isAlive());

		// Il barbaro è molto più resistente
		player = FactoryCreature.createBarbarian();
		ogre.fight(player);
		ogre.fight(player);
		assertTrue(player.isAlive());
		ogre.fight(player);
		ogre.fight(player);
		ogre.fight(player);
		assertFalse(player.isAlive());

		// Il paladino è una via di mezzo
		player = FactoryCreature.createPaladin();
		ogre.fight(player);
		ogre.fight(player);
		assertTrue(player.isAlive());
		ogre.fight(player);
		assertFalse(player.isAlive());
	}

	@Test
	public void testFight() {
		// Combattere con un'arma comporta una perdita di salute minore ad ogni colpo
		Warrior player = FactoryCreature.createPaladin();
		int startLifePoints = player.getLifePoints();
		Ogre ogre = new Ogre();
		
		// Senza armi
		ogre.fight(player);
		int differenceWithoutWeapon = startLifePoints - player.getLifePoints();
		
		player.addLifePoints(differenceWithoutWeapon);
		
		// Con una spada
		player.setWeapon(new Weapon("Sword", 10));
		ogre.fight(player);
		int differenceWithWeapon = startLifePoints - player.getLifePoints();
		player.addLifePoints(differenceWithWeapon);
		
		// Con un'arma inferiore
		player.setWeapon(new Weapon("Dagger", 20));
		ogre.fight(player);
		int differenceWithWeakerWeapon = startLifePoints - player.getLifePoints();
		player.addLifePoints(differenceWithWeakerWeapon);
		
		// E infine il guerriero muore
		ogre.fight(player);
		ogre.fight(player);
		ogre.fight(player);
		ogre.fight(player);
		ogre.fight(player);
		ogre.fight(player);
		ogre.fight(player);
		
		assertTrue(differenceWithWeapon < differenceWithoutWeapon);
		assertTrue(differenceWithWeakerWeapon < differenceWithoutWeapon);
		assertTrue(differenceWithWeapon < differenceWithWeakerWeapon);
		
		assertFalse(player.isAlive());
	}
}
